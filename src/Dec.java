public class Dec extends NumberSystemConverter{

    public Dec(String val){
        int num = validate("[-]?[0-9]+", val);
        setNumber(num);
    }

    public Dec(Integer num){
        setNumber(num);
    }

    @Override
    public int getBase(){
        return 10;
    }
}
