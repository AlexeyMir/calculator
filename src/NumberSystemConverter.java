public abstract class NumberSystemConverter {
    private int field;

    public NumberSystemConverter(){}

    public void setNumber(int val){
        this.field = val;
    }

    public int getNumber(){
        return this.field;
    }

    public abstract int getBase();

    protected int validate(String str, String val){
        if (!val.matches(str)){
            throw new RuntimeException("Данное число не представлено в текущей системе счисления!");
        }
        return Integer.parseInt(val, getBase());
    }

    public String toString(){
        return getBase() + " " + getThisNumber();
    }

    private String getThisNumber() {
        switch (getBase()) {
            case 2: return " " + Integer.toBinaryString(getNumber());
            case 8: return " " + Integer.toOctalString(getNumber());
            case 10: return String.valueOf(getNumber());
            case 16: return Integer.toHexString(getNumber()).toUpperCase();
        }
        throw new RuntimeException("Данное число не представлено в текущей системе счисления!");
    }

    public String write(){
        switch (getBase()){
            case 2:{
                return 8 + "  " + Integer.toOctalString(getNumber()) + "\n" +
                        10 + " " + getNumber() + "\n" +
                        16 + " " + Integer.toHexString(getNumber()).toUpperCase();
            }
            case 8:{
                return 2 + "  " + Integer.toBinaryString(getNumber()) + "\n" +
                        10 + " " + getNumber() + "\n" +
                        16 + " " + Integer.toHexString(getNumber()).toUpperCase();
            }
            case 10:{
                return 2 + "  " + Integer.toBinaryString(getNumber()) + "\n" +
                        8 + "  " + Integer.toOctalString(getNumber()) + "\n" +
                        16 + " " + Integer.toHexString(getNumber()).toUpperCase();
            }
            case 16:{
                return 2 + "  " + Integer.toBinaryString(getNumber()) + "\n" +
                        8 + "  " + Integer.toOctalString(getNumber()) + "\n" +
                        10 + " " + getNumber();
            }
        }
        throw new RuntimeException("Данное число не представлено в текущей системе счисления!");
    }
}
