public class Bin extends NumberSystemConverter{

    public Bin(String val){
        int num = validate("[-]?[0-1]+", val);
        setNumber(num);
    }

    public Bin(Integer num){
        setNumber(num);
    }

    @Override
    public int getBase(){
        return 2;
    }
}
