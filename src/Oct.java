public class Oct extends NumberSystemConverter{

    public Oct(String val){
        int num = validate("[-]?[0-7]+", val);
        setNumber(num);
    }

    public Oct(Integer num){
        setNumber(num);
    }

    @Override
    public int getBase(){
        return 8;
    }
}
