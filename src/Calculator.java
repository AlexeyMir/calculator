public class Calculator implements INumOperations{

    private NumberSystemConverter resultNumber(int base, Integer str){
        switch (base) {
            case 2: return new Bin(str);
            case 8: return new Oct(str);
            case 10: return new Dec(str);
            case 16: return new Hex(str);
            default:
                throw new RuntimeException("Неопределённая система");
        }
    }

    @Override
    public NumberSystemConverter summation(NumberSystemConverter number1, NumberSystemConverter number2) {
        return resultNumber(number1.getBase(), number1.getNumber() + number2.getNumber());
    }

    @Override
    public NumberSystemConverter subtraction(NumberSystemConverter number1, NumberSystemConverter number2) {
        return resultNumber(number1.getBase(), number1.getNumber() - number2.getNumber());
    }

    @Override
    public NumberSystemConverter multiplication(NumberSystemConverter number1, NumberSystemConverter number2) {
        return resultNumber(number1.getBase(), number1.getNumber() * number2.getNumber());
    }

    @Override
    public NumberSystemConverter division(NumberSystemConverter number1, NumberSystemConverter number2) {
        if (number2.getNumber() != 0){
            return resultNumber(number1.getBase(), number1.getNumber() / number2.getNumber());
        }
        throw new RuntimeException("На 0 делить нельзя!");
    }
}
