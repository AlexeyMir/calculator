import java.util.List;
import java.util.Scanner;

public class Main {
    private static final List<String> number_system_selection_menu = List.of(
            "Выберите систему счисления: ",
            "1. Двоичная",
            "2. Восьмеричная",
            "3. Десятичная",
            "4. Шестнадцатеричная",
            "5. Назад");
    private static final List<String> operation_selection_menu = List.of(
            "+",
            "-",
            "*",
            "/",
            "5. Назад");
    private static NumberSystemConverter number1;
    private static NumberSystemConverter number2;

    static void mode() {
        boolean a = true;
        while (a) {
            number_system_selection_menu.forEach(System.out::println);
            Scanner in = new Scanner(System.in);
            try {
                switch (in.next()) {
                    case "1": {
                        System.out.println("Введите первое число: ");
                        number1 = new Bin(in.next());
                        System.out.println("Введите второе число: ");
                        number2 = new Bin(in.next());
                        System.out.println("Выберите операцию: ");
                        operation(number1, number2);
                        break;
                    }
                    case "2": {
                        System.out.println("Введите первое число: ");
                        number1 = new Oct(in.next());
                        System.out.println("Введите второе число: ");
                        number2 = new Oct(in.next());
                        System.out.println("Выберите операцию: ");
                        operation(number1, number2);
                        break;
                    }
                    case "3": {
                        System.out.println("Введите первое число: ");
                        number1 = new Dec(in.next());
                        System.out.println("Введите второе число: ");
                        number2 = new Dec(in.next());
                        System.out.println("Выберите операцию: ");
                        operation(number1, number2);
                        break;
                    }
                    case "4": {
                        System.out.println("Введите первое число: ");
                        number1 = new Hex(in.next());
                        System.out.println("Введите второе число: ");
                        number2 = new Hex(in.next());
                        System.out.println("Выберите операцию: ");
                        operation(number1, number2);
                        break;
                    }
                    case "5": {
                        a = false;
                        break;
                    }
                    default: {
                        System.out.println("Такой команды не существует\n"); break;
                    }
                }
            }
            catch (Exception e) {
                System.out.println(e.getMessage() + " Попробуйте заново.\n");
            }
        }
    }

    static void operation(NumberSystemConverter numSystem1, NumberSystemConverter numSystem2) {
        try {
            operation_selection_menu.forEach(System.out::println);
            Scanner in = new Scanner(System.in);
            Calculator calc = new Calculator();
            NumberSystemConverter system;
            String operation = in.next();
            switch (operation) {
                case "+": {
                    numSystem1 = calc.summation(numSystem1, numSystem2);
                    System.out.println(numSystem1);
                    System.out.println(numSystem1.write());
                    break;
                }
                case "-": {
                    system = calc.subtraction(numSystem1, numSystem2);
                    System.out.println(system);
                    System.out.println(system.write());
                    break;
                }
                case "*": {
                    system = calc.multiplication(numSystem1, numSystem2);
                    System.out.println(system);
                    System.out.println(system.write());
                    break;
                }
                case "/": {
                    system = calc.division(numSystem1, numSystem2);
                    System.out.println(system);
                    System.out.println(system.write());
                    break;
                }
                case "5": break;
                default: {
                    System.out.println("Такой команды не существует\n");
                    break;
                }
            }
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void main(String[] args) {
        boolean a = true;
        while (a) {
            System.out.println("1. Начать");
            System.out.println("2. Выход");
            Scanner in = new Scanner(System.in);
            switch (in.next()) {
                case "1": {
                    mode();
                    break;
                }
                case "2":
                    a = false;
                    break;
                default: {
                    System.out.println("Такой команды не существует\n");
                    break;
                }
            }
        }
    }
}