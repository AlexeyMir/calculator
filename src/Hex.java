public class Hex extends NumberSystemConverter{

    public Hex(String val){
        int num = validate("[-]?[0-9A-Fa-f]+", val);
        setNumber(num);
    }

    public Hex(Integer num){
        setNumber(num);
    }

    @Override
    public int getBase(){
        return 16;
    }
}
