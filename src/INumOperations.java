public interface INumOperations {

    NumberSystemConverter summation(NumberSystemConverter number1, NumberSystemConverter number2);
    NumberSystemConverter subtraction(NumberSystemConverter number1, NumberSystemConverter number2);
    NumberSystemConverter multiplication(NumberSystemConverter number1, NumberSystemConverter number2);
    NumberSystemConverter division(NumberSystemConverter number1, NumberSystemConverter number2);
}
